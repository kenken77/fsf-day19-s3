(function(){
    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);
    
    UploadCtrl.$inject = ["Upload"];
    
    function UploadCtrl(Upload){
        var vm = this;
        vm.imgFile = null;
        vm.status = {
            message: "",
            code: 0 
        }
        // public functions for the view
        vm.upload = upload;
        vm.uploadS3 = uploadS3;
        
        function upload(){
            console.log("Upload ...");
            Upload.upload({
                url: '/upload',
                data: {
                    "img-file": vm.imgFile,
                }
            }).then(function(response){
                vm.fileUrl = response.data;
                vm.status.message= "Upload successful";
                vm.status.code = response.status;
                console.log(response.status);
            }).catch(function(err){
                vm.status.message= "Failed";
                vm.status.code = err.status;
            });
        }

        function uploadS3(){
            console.log("Upload S3...");
            Upload.upload({
                url: '/uploadS3',
                data: {
                    "img-file": vm.imgFile,
                }
            }).then(function(response){
                vm.fileUrl = response.data;
                vm.status.message= "Upload successful";
                console.log(response.status);
                vm.status.code = response.status;

            }).catch(function(err){
                vm.status.message= "Failed";
                vm.status.code = err.status;
            });
        }

    }
    

})();